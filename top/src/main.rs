use aoeu;
use blar;

fn main() {
    let x = aoeu::add(1, 2);
    let y = blar::add(x, 2);
    println!("Hello, world! {} {}", x, y);
}
