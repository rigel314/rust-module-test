pub fn add(left: usize, right: usize) -> usize {
    left + right
}

pub fn asdf(mut left: usize) -> usize {
    let mut sum: usize = 0;
    while left > 0 {
        sum += left;
        left -= 1;
    }
    sum
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}
